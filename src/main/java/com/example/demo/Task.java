package com.example.demo;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="employees")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
allowGetters = true)
public class Task {

/**
*
*/
private static final long serialVersionUID = 1L;

@Id

@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@Column(name="USER_NAME")
private String name;

@Column(name="LOAN_ID")
private String lid;

@Column(name="LOAN_AMOUNT")
private int amount;

@Column(name="NO_OF_MONTHS")
private int month;
@Column(name="TYPES_OF_LOAN")
private String typeofloan;
@Column(name="INTEREST")
public int interest;



public String getTypeofloan() {
	return typeofloan;
}

public void setTypeofloan(String typeofloan) {
	this.typeofloan = typeofloan;
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getLid() {
	return lid;
}
public Integer getInterest() {
	return interest;
}

public void setInterest(Integer interest) {
	this.interest = interest;
}


public void setLid(String lid) {
	this.lid = lid;
}

public int getAmount() {
	return amount;
}

public void setAmount(int amount) {
	this.amount = amount;
}

public int getMonth() {
	return month;
}

public void setMonth(int month) {
	this.month = month;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}


}